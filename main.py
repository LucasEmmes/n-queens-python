import chess

def solve(board: 'chess.ChessBoard', depth: int = 0) -> 'chess.ChessBoard':
    if depth == 8: return board
    for i in range(8):
        board.add_queen(depth, i)
        valid = board.isValid()
        if valid:
            result = solve(board, depth+1)
            if result is not None:
                return result
        board.remove_queen(depth, i)
    return None

def main():
    board = chess.ChessBoard()
    result = solve(board)
    print(result)

if __name__ == "__main__": main()