from dataclasses import dataclass
from typing import Set

@dataclass(frozen=True)
class Queen:
    x: int
    y: int

class ChessBoard:
    def __init__(self) -> None:
        self.queens: Set['Queen'] = set()

    def __str__(self) -> str:
        grid = [["0" for i in range(8)] for i in range(8)]
        for queen in self.queens:
            grid[queen.y][queen.x] = "X"
        
        return "\n".join([str(row) for row in grid])

    def __repr__(self) -> str:
        return self.__str__()

    def add_queen(self, x: int, y: int) -> None:
        self.queens.add(Queen(x, y))

    def remove_queen(self, x, y) -> None:
        self.queens.remove(Queen(x, y))

    def isValid(self) -> bool:
        for queen in self.queens:
            for other_queen in self.queens:
                if queen is other_queen: continue
                if queen.x == other_queen.x or queen.y == other_queen.y: return False
                if abs(queen.x - other_queen.x) == abs(queen.y - other_queen.y): return False
        return True